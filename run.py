#!/usr/bin/env python3

__ASSISTANT_NAME__ = "Assistant"
__USER_NAME__ = "User"

import os
import json
import queue
import sys
import sounddevice as sd
import threading
from vosk import Model, KaldiRecognizer

model_path = os.path.expanduser("vosk/vosk-model-en-us-0.22")
music_path = os.path.expanduser("~/Music")
player = "mpv --volume=30 --shuffle"

should_listen = threading.Event()
q = queue.Queue()

def speak(text):
    try:
        print(f'{__ASSISTANT_NAME__}: "{text}"')
        should_listen.clear()
        os.system(f'echo "{text}" | festival --tts')
        should_listen.set()
    except Exception as e:
        print(f'Error: {e}')

def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    if should_listen.is_set():
        q.put(bytes(indata))

def process_command(command):
    print(f'{__USER_NAME__}: "{command}"')
    command = command.lower()
    if "exit" in command:
        speak("Goodbye!")
        raise SystemExit
    elif "play some music" in command:
        speak("Playing music")
        os.system(f'{player} {music_path} &')
    elif "stop the music" in command:
        speak("Stopping music")
        os.system(f'pkill -f "{player} {music_path}"')
    elif "time" in command:
        import time
        current_time = time.strftime("%H:%M")
        speak(f"The current time is {current_time}")
    # else:
    #     speak("Sorry, I don't understand that command.")

def main():
    try:
        default_device = sd.query_devices(None, 'input')
        samplerate = int(default_device['default_samplerate'])

        model = Model(model_path)
        recognizer = KaldiRecognizer(model, samplerate)

        speak("How can I assist?")

        with sd.RawInputStream(samplerate=samplerate, blocksize=8000, dtype='int16', channels=1, callback=callback):
            print("##################################")
            print("Press Ctrl+C to stop the recording")
            print("##################################")

            while True:
                data = q.get()
                if recognizer.AcceptWaveform(data):
                    result = json.loads(recognizer.Result())
                    if (result['text']):
                        process_command(result["text"])
                # else:
                #     partial_result = json.loads(recognizer.PartialResult())
                #     print(f"Partial result: {partial_result['partial']}")

    except KeyboardInterrupt:
        print("\nDone")
        exit(0)
    except Exception as e:
        print(f"Exception occurred: {e}")

if __name__ == "__main__":
    main()
