# VoiceAssist for Linux

Simple python script that listens for commands.

## How to use

```sh
$ cd ~/Projects # or wherever you keep things like this
$ git clone --depth 1 https://gitlab.com/cprn/voiceassist.git
$ cd voiceassist
$ make
```

The required English voice model will download to `vosk` directory on 1st run.
It self manages a virutal environment to avoid mangling system python libraries.
Requires python3.

## Commands

The only supported commands for now are:
- what time is it
- play some music (runs `mpv`)
- stop the music (kills `mpv`)
- exit

## How to remove
```sh
$ rm -rf ~/Projects/voiceassist
```
