.PHONY: all run clean

all: run

run: | .venv vosk/vosk-model-en-us-0.22
	eval source .venv/bin/activate && python3 run.py

clean:
	rm -rf vosk .venv

vosk:
	mkdir vosk

vosk/vosk-model-en-us-0.22: | vosk
	cd vosk && wget https://alphacephei.com/vosk/models/vosk-model-en-us-0.22.zip
	cd vosk && unzip vosk-model-en-us-0.22.zip
	cd vosk && rm *zip

.venv:
	python3 -m venv .venv
	eval source .venv/bin/activate && pip install vosk sounddevice
